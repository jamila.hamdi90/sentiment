# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 04:13:22 2020

@author: dell
"""

import pandas as pd
import numpy as np
import re
import pickle
import nltk
# from nltk.corpus import stopwords
import keras
from keras.preprocessing.text import one_hot
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers.core import Activation, Dropout, Dense
from keras.layers import Flatten, LSTM, Bidirectional, Conv1D, GRU
from keras.layers import GlobalMaxPooling1D, GlobalAveragePooling1D
from keras.layers.embeddings import Embedding
from sklearn.model_selection import train_test_split
from keras.preprocessing.text import Tokenizer
from sklearn.preprocessing import LabelBinarizer

from keras.preprocessing.sequence import pad_sequences
import keras.layers
from keras.layers import Reshape
from keras.layers import Input
import matplotlib.pyplot as plt

print("INFO: Loading Dataset")
# Load dataset
#dataset = pd.read_csv("dataset/iseardataset.csv")
dataset = pd.read_csv('aspectCorpus.csv', sep=';', encoding="Windows-1256", header= None)
dataset = dataset.loc[:, [0, 1, 2, 3, 4]]
dataset = dataset.rename(index=str, columns={0:'sentiment', 1:'aspect_category', 2:'review',3:'aspect_terms', 4:'sentiment_terms'})

#print(df.iloc[0])


def preprocess_text(sen):
    import re
    import string
    arabic_punctuations = '''`÷×#@؛<>_()*&^%][ـ،/:"؟.,'{}~¦+|!”…“–ـ'''
    english_punctuations = string.punctuation
    punctuations_list = arabic_punctuations + english_punctuations
    
    arabic_diacritics = re.compile("""
                                 ّ    | # Tashdid
                                 َ    | # Fatha
                                 ً    | # Tanwin Fath
                                 ُ    | # Damma
                                 ٌ    | # Tanwin Damm
                                 ِ    | # Kasra
                                 ٍ    | # Tanwin Kasr
                                 ْ    | # Sukun
                                 ـ     # Tatwil/Kashida
                             """, re.VERBOSE)
    
    
    def normalize_arabic(text):
        text = re.sub("[إأآا]", "ا", text)
        text = re.sub("ى", "ي", text)
        text = re.sub("ؤ", "ء", text)
        text = re.sub("ئ", "ء", text)
        text = re.sub("ة", "ه", text)
        text = re.sub("گ", "ك", text)
        return text
    
    
    def remove_diacritics(text):
        text = re.sub(arabic_diacritics, '', text)
        return text
    
    
    def remove_punctuations(text):
        translator = str.maketrans('', '', punctuations_list)
        return text.translate(translator)
    
    
    def remove_repeating_char(text):
        return re.sub(r'(.)\1+', r'\1\1', text)  
    
        
    def remove_urls(string):
        regex = re.compile(r"(http|https|ftp)://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")
        return re.sub(regex, ' ', string)
    
    def remove_dates(string):
    
        OutputList = filter(lambda ThisWord: not re.match('^(?:(?:[0-9]{2}[:\/,]){2}[0-9]{2,4}|am|pm)$', ThisWord),
        string)
        for ThisValue in OutputList:
            print (ThisValue)
  
    import re
    def remove_non_arabic_symbols(string): #marche seulement pour les mots arabes
        return re.sub(r'[^\u0600-\u06FF]', ' ', string)    
        
    
    def remove_numbers(string):   #supprime meme les nombre dans une mot comme m9ate3 devient m ate
        regex = re.compile(r"(\d|[\u0660\u0661\u0662\u0663\u0664\u0665\u0666\u0667\u0668\u0669])+")
        return re.sub(regex, ' ', string)
        
    def normalize(string):    #à réecrire
            regex = re.compile(r'[3]')
            string = re.sub(regex, 'aa', string)
            regex = re.compile(r'[9 ]')
            string = re.sub(regex, 'q', string)
            regex = re.compile(r'[5]')
            string = re.sub(regex, 'kh', string)
            regex = re.compile(r'[8]')
            string = re.sub(regex, 'gh', string)
            regex = re.compile(r'[7]')
            string = re.sub(regex, 'ha', string)
            return string
    
    
    def removeonecarc(list):  
        for index, value in enumerate(list): #supprime une chaine qui contient un seule caractère
            if (len(value))== 1:
              list.remove(value)
              print(index, value)
        return list
    
        
    
    def remove_non_arabic_words(string):
        return ' '.join([word for word in string.split() if not re.findall(
            r'[^\s\u0621\u0622\u0623\u0624\u0625\u0626\u0627\u0628\u0629\u062A\u062B\u062C\u062D\u062E\u062F\u0630\u0631\u0632\u0633\u0634\u0635\u0636\u0637\u0638\u0639\u063A\u0640\u0641\u0642\u0643\u0644\u0645\u0646\u0647\u0648\u0649\u064A]',
            word)])
    
    def remove_extra_whitespace(string):
        for ligne in string:
            red = re.sub(' +',' ',ligne)
        return red
    
    text2 = remove_urls(str(sen))
    #print(text2)
    
    text3 = remove_numbers(text2)  #on doit pas l'utiliser dans le corpus en latin
    #print(text3)
    
    text4 = remove_punctuations(text3)
    #print(text4)
    text5 = remove_diacritics(text4)   
    #print(text5)
    text6 = remove_non_arabic_symbols(text5)
    text7 = text6.strip()

    text8 = remove_repeating_char(text7)

    sentence = text8 

    return sentence

print("INFO: Preprocessing Dataset")

#dataset.head(100)
# reviews_train.columns
#print(dataset.groupby('aspect_category').size().sort_values(ascending=False))
#how many categories
#print("number of categories",dataset.aspect_category.nunique())

label = dataset['sentiment'].values

aspect_terms= dataset['aspect_terms']
aspect_category= dataset['aspect_category'].values


X = []
review = list(dataset['review'])

for sen in review:
    X.append(preprocess_text(sen))
#y = dataset['label']
##
# encode the aspect_category :

label = dataset['sentiment'].values
review = dataset['review']
aspect_terms= dataset['aspect_terms']
aspect_category= dataset['aspect_category'].values
sentiment_terms = dataset['sentiment_terms']
from sklearn.model_selection import train_test_split
# encode the aspect_category :
import keras
from sklearn import preprocessing
# encode class values as integers
encoder = preprocessing.LabelEncoder()
encoder.fit(aspect_category.astype(str))
encoded_Y = encoder.transform(aspect_category.astype(str))
# convert integers to dummy variables (i.e. one hot encoded)
dummy_category = keras.utils.to_categorical(encoded_Y)
print(dummy_category.shape)
aspect_train, aspect_test, y_train, y_test = train_test_split(aspect_terms, dummy_category, test_size=0.25, random_state=1000)

#Tokeniser
from keras.preprocessing.text import Tokenizer
vocab_size = 40000 # We set a maximum size for the vocabulary
tokenizer = Tokenizer(num_words=vocab_size, lower=False)

tokenizer.fit_on_texts(review.astype(str))
aspect_train = tokenizer.texts_to_sequences(aspect_train.astype(str))
aspect_test = tokenizer.texts_to_sequences(aspect_test.astype(str))
#aspect_tokenized = pd.DataFrame(tokenizer.texts_to_matrix(aspect_terms.astype(str)))
word_index = tokenizer.word_index
vocab_size = len(word_index) + 1  # Adding 1 because of reserved 0 index
print('Found %s unique tokens.' % len(word_index))
#pad_sequences
maxlen = 1000
aspect_train = pad_sequences(aspect_train, padding='post', maxlen=maxlen)
aspect_test = pad_sequences(aspect_test, padding='post', maxlen=maxlen)

print('Shape of data tensor:', aspect_train.shape)
print('Shape of data tensor:', y_train.shape)
print('Shape of data tensor:', aspect_test.shape)
print('Shape of data tensor:', y_test.shape)
import nltk
nltk.download('punkt')
#wtxt1 = open('arabeF.txt', encoding="Windows-1256").read()
#wtokens = nltk.word_tokenize(wtxt1)
import logging 
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logging.info ("Done reading data file")
from gensim.models import Word2Vec
aspect_terms = str(aspect_terms)
model = Word2Vec ([aspect_terms], size=300, window=10,  iter=10, min_count=1, workers=10, sg = 1) #min_count means ignore all words with total frequency lower than this. 
model.save("word2vec.model")
print("Finished!")
word_vectors = Word2Vec.load("word2vec.model")
#len(model.wv.vocab)
num_words=40000
EMBEDDING_DIM=300
vocab_size=40000
vocabulary_size=min(vocab_size,num_words)
embedding_matrix = np.zeros((vocabulary_size, EMBEDDING_DIM))
print('Shape of embedding_matrix:', embedding_matrix.shape)

for word, i in word_index.items():
    if i>=num_words:
        continue
    try:
        embedding_vector = word_vectors[word]
        embedding_matrix[i] = embedding_vector
    except KeyError:
        embedding_matrix[i]=np.random.normal(0,np.sqrt(0.25),EMBEDDING_DIM)
del(word_vectors)

#Embedding
embedding_layer = Embedding(vocabulary_size,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            trainable=True)

# Convolutional Neural Networks (CNN)
from keras.models import Sequential
from keras import layers

embedding_dim = 300
#je dois essayer de changer embedding zite to 300
model = Sequential()
model.add(embedding_layer)
#model.add(layers.Conv1D(23, 5, activation='relu'))
#model.add(layers.Conv1D(64, 5, activation='relu'))
model.add(layers.Conv1D(128, 5, activation='relu'))
model.add(layers.GlobalMaxPooling1D())
#model.add(layers.Conv1D(128, 5, activation='relu'))
#model.add(layers.GlobalMaxPooling1D())
model.add(layers.Dense(10, activation='relu'))
#model.add(Dropout(0.5))
model.add(layers.Dense(20, activation='softmax'))
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['categorical_accuracy'])
model.summary()

es_callback = keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
#aspect_train = pd.DataFrame(aspect_train) 
#aspect_tokenized = pd.DataFrame(tokenizer.texts_to_matrix(aspect_terms))

history = model.fit(aspect_train, y_train, epochs=100,validation_split=0.2, batch_size=64, callbacks=[es_callback])
#aspect_categories_model.predict(aspect_test) 
# calcul F score
# calcul F score
#from sklearn.metrics import f1_score
#y_pred = aspect_categories_model.predict(aspect_test)
# Save model, classes_names and tokenizer file
model.save("model_final1.model")
np.save("class_namestest1.npy", encoder.classes_)

with open('encoder1.pickle', 'wb') as f:
    pickle.dump(encoder, f)

with open('tokenizertest1.pickle', 'wb') as handle:
    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
print("INFO: Saving models done")

###############################################################################################
#modèle de sentiment
sentiment_terms= dataset['sentiment_terms']
sentiment= dataset['sentiment']

from sklearn.model_selection import train_test_split
# encode the aspect_category :
import keras
from sklearn import preprocessing
# encode class values as integers
label_encoder_2 = preprocessing.LabelEncoder()
#sentiment =str(sentiment)
#label_encoder_2.fit_transform(sentiment)
encoded_Y = label_encoder_2.fit_transform(dataset.sentiment)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_sentiment = keras.utils.to_categorical(encoded_Y)
#print(dummy_sentiment.shape)
#aspect_train, aspect_test, y_train, y_test = train_test_split(sentiment_terms, dummy_sentiment, test_size=0.25, random_state=1000)
sentiment_train, sentiment_test, y_train1, y_test1 = train_test_split(sentiment_terms, dummy_sentiment, test_size=0.25, random_state=1000)
#sentiment_tokenized = pd.DataFrame(tokenizer.texts_to_matrix(str(sentiment_terms)))
#Tokeniser
from keras.preprocessing.text import Tokenizer
vocab_size = 40000 # We set a maximum size for the vocabulary
tokenizer = Tokenizer(num_words=vocab_size, lower=False)

tokenizer.fit_on_texts(review.astype(str))
sentiment_train = tokenizer.texts_to_sequences(sentiment_train.astype(str))
sentiment_test = tokenizer.texts_to_sequences(sentiment_test.astype(str))
#aspect_tokenized = pd.DataFrame(tokenizer.texts_to_matrix(aspect_terms.astype(str)))
word_index = tokenizer.word_index
vocab_size = len(word_index) + 1  # Adding 1 because of reserved 0 index

#pad_sequences
maxlen = 1000
sentiment_train = pad_sequences(sentiment_train, padding='post', maxlen=maxlen)
sentiment_test = pad_sequences(sentiment_test, padding='post', maxlen=maxlen)
#print('Shape of data tensor:', sentiment_train.shape)

import nltk
nltk.download('punkt')
sentiment_terms = str(sentiment_terms)
#wtxt1 = open('sentiment_terms', encoding="Windows-1256").read()
#wtokens = nltk.word_tokenize(wtxt1)
import logging 
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logging.info ("Done reading data file")
from gensim.models import Word2Vec
model = Word2Vec ([sentiment_terms], size=300, window=10,  iter=10, min_count=1, workers=10, sg = 1) #min_count means ignore all words with total frequency lower than this. 
model.save("word2vec.model")
#print("Finished!")
word_vectors = Word2Vec.load("word2vec.model")
#len(model.wv.vocab)
num_words=40000
EMBEDDING_DIM=300
vocab_size=40000
vocabulary_size=min(vocab_size,num_words)
embedding_matrix = np.zeros((vocabulary_size, EMBEDDING_DIM))
print('Shape of embedding_matrix:', embedding_matrix.shape)

for word, i in word_index.items():
    if i>=num_words:
        continue
    try:
        embedding_vector = word_vectors[word]
        embedding_matrix[i] = embedding_vector
    except KeyError:
        embedding_matrix[i]=np.random.normal(0,np.sqrt(0.25),EMBEDDING_DIM)
del(word_vectors)

#Embedding
embedding_layer1 = Embedding(vocabulary_size,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            trainable=True)

#je dois essayer de changer embedding zite to 300
sentiment_model = Sequential()
sentiment_model.add(embedding_layer1)
#model.add(layers.Conv1D(23, 5, activation='relu'))
#model.add(layers.Conv1D(64, 5, activation='relu'))
sentiment_model.add(layers.Conv1D(128, 5, activation='relu'))
sentiment_model.add(layers.GlobalMaxPooling1D())
#model.add(layers.Conv1D(128, 5, activation='relu'))
#model.add(layers.GlobalMaxPooling1D())
sentiment_model.add(layers.Dense(10, activation='relu'))
#model.add(Dropout(0.5))
sentiment_model.add(layers.Dense(5, activation='softmax'))
sentiment_model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['categorical_accuracy'])
sentiment_model.summary()



import keras
from keras.callbacks import EarlyStopping
es_callback = keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
history = sentiment_model.fit(sentiment_train, y_train1, epochs=100, validation_split=0.2, batch_size=64, callbacks=[es_callback])
# calcul F score
#sentiment_model.predict(sentiment_test)
model.save("model_final2.model")
np.save("class_namestest2.npy", label_encoder_2.classes_)

with open('encoder.pickle', 'wb') as f:
    pickle.dump(label_encoder_2, f)

with open('tokenizertest2.pickle', 'wb') as handle:
    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
print("INFO: Saving models done")
