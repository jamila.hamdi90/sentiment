# -*- coding: utf-8 -*-
"""
Created on Sat May 30 20:22:12 2020

@author: dell
"""

import numpy as np
import pandas as pd
import tensorflow as tf
import pickle
import re
from flask import Flask, render_template, request

app = Flask(__name__)


@app.route("/")
def home():
    return render_template("home.html")


# Tokenize and pad sentence
@app.route('/predict', methods=['GET', 'POST'])
def predict():
    if request.form['submit_button'] == 'predict at aspect':

        # Your input sentence
        data = request.form['message']
        # data = "المغازة العامة ادلالها ما يوفاش"
        aspectterms = 'سوسة'
        sentimentterms = 'يوفاش'
        sentence = [data]
        aspect = [aspectterms]
        sentiment = [sentimentterms]
        # sentence = ['When stole book in class and the teacher caught me the rest of the class laughed at my attempt ']

        print("[INFO]: Loading Classes")
        # Load class names
        classNames1 = np.load("./class_namestest1.npy", allow_pickle=True)

        # Load tokenizer pickle file

        print("[INFO]: Loading Tokens")
        with open('./tokenizertest1.pickle', 'rb') as handle:
            Tokenizer1 = pickle.load(handle)
        # load the encoder
        with open('encoder1.pickle', 'rb') as f:
            encoder = pickle.load(f)
        # Load model

        print("[INFO]: Loading Model")
        model1 = tf.keras.models.load_model("./model_final1.model")
        ##
        classNames2 = np.load("./class_namestest2.npy", allow_pickle=True)

        # Load tokenizer pickle file

        print("[INFO]: Loading Tokens")
        with open('./tokenizertest2.pickle', 'rb') as handle:
            Tokenizer2 = pickle.load(handle)

        # load the encoder
        with open('encoder.pickle', 'rb') as f:
            label_encoder_2 = pickle.load(f)

        # Load model

        print("[INFO]: Loading Model")
        model2 = tf.keras.models.load_model("./model_final2.model")

        print("[INFO]: Preprocessing")

        # Preprocess Text

        def preprocess_text(sentence):
            import string
            arabic_punctuations = '''`÷×#@؛<>_()*&^%][ـ،/:"؟.,'{}~¦+|!”…“–ـ'''
            english_punctuations = string.punctuation
            punctuations_list = arabic_punctuations + english_punctuations

            def lower(text):
                res = ''
                for ligne in text:
                    res = res + ligne.lower()
                return res

            def remove_punctuations(text):
                translator = str.maketrans('', '', punctuations_list)
                return text.translate(translator)

            def remove_repeating_char(text):
                return re.sub(r'(.)\1+', r'\1\1', text)

            def remove_extra_whitespace(string):
                red = re.sub(' +', ' ', string)
                return red

            def remove_urls(string):
                string = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', string, flags=re.MULTILINE)
                return string

            def remove_dates(string):
                OutputList = filter(
                    lambda ThisWord: not re.match('^(?:(?:[0-9]{2}[:\/,]){2}[0-9]{2,4}|am|pm)$', ThisWord),
                    string)
                for ThisValue in OutputList:
                    print(ThisValue)

            def remove_non_arabic_symbols(string):  # marche seulement pour les mots arabes
                return re.sub(r'[^\u0600-\u06FF]', ' ', string)

            def remove_numbers(string):  # supprime meme les nombre dans une mot comme m9ate3 devient m ate
                regex = re.compile(r"(\d|[\u0660\u0661\u0662\u0663\u0664\u0665\u0666\u0667\u0668\u0669])+")
                return re.sub(regex, ' ', string)

            def removeonecarc(string):
                for ligne1 in string:
                    resul = re.sub(r"\s+[a-zA-Z]\s+", " ", ligne1)  # Removing a Single Character
                return resul

            def remove_numbre_not_att(string):  # marche seulement pour les mots arabes
                return re.sub(r'\b[0-9]+\b\s*', '', string)

            text2 = remove_urls(sentence)
            text3 = remove_numbre_not_att(text2)
            text4 = remove_punctuations(text3)
            text6 = lower(text4)
            text7 = remove_extra_whitespace(text6)
            for ligne1 in [text7]:
                resul = re.sub(r"\s+[a-zA-Z]\s+", " ", ligne1)  # Removing a Single Character 11

            for ligne in [resul]:
                resull = re.sub(r"\s+[a-zA-Z]\s+", " ", ligne)  # Removing a Single Character 22

            sentence = resull

            return sentence

        # if request.post == "POST":
        # message = request['message']
        MAX_LENGTH = 100
        Tokenizer1.fit_on_texts(sentence)
        aspect_processed = Tokenizer1.texts_to_sequences(aspect)
        # aspect_processed = np.array(aspect_processed)
        aspect_padded = tf.keras.preprocessing.sequence.pad_sequences(aspect_processed, padding='post',
                                                                      maxlen=MAX_LENGTH)
        Tokenizer2.fit_on_texts(sentence)
        sentiment_processed = Tokenizer2.texts_to_sequences(sentiment)
        # sentiment_processed = np.array(sentiment_processed)
        sentiment_padded = tf.keras.preprocessing.sequence.pad_sequences(sentiment_processed, padding='post')
        #
        # print("""[INFO]: Prediction\n\t{}""".format(sentence[0]))
        # sen = "المغازة العامة ادلالها ما يوفاش"
        # new_review_aspect_terms = 'سوسة'
        new_review_aspect_tokenized = pd.DataFrame(Tokenizer1.texts_to_matrix(aspectterms))
        new_review_sentiment_tokenized = pd.DataFrame(Tokenizer2.texts_to_matrix(sentimentterms))

        new_review_category1 = encoder.inverse_transform(model1.predict_classes(new_review_aspect_tokenized))
        new_review_sentiment = label_encoder_2.inverse_transform(model2.predict_classes(new_review_sentiment_tokenized))

        result_list1 = new_review_category1.tolist()
        result_list2 = new_review_sentiment.tolist()

        pred1 = classNames1[np.argmax(result_list1)]

        pred2 = classNames2[np.argmax(result_list2)]

        print("Review " + str(data) + " is expressing a  " + classNames2[np.argmax(result_list1)] + " opinion about " +
              classNames1[np.argmax(result_list1)])
        result_list = result_list1 + result_list2
        pred = pred1 + pred2
        # import pdb;pdb.set_trace()
        # resultt= new_r eview_category1.all() + new_review_sentiment.all()
        return render_template("result.html", prediction=pred1, prediction1=pred2, data=data)

    else:
        # Your input sentence
        data = request.form['message']
        sentence = [data]
        # sentence = ['When stole book in class and the teacher caught me the rest of the class laughed at my attempt ']

        print("[INFO]: Loading Classes")
        # Load class names
        classNames = np.load("./class_namestest.npy", allow_pickle=True)

        # Load tokenizer pickle file

        print("[INFO]: Loading Tokens")
        with open('./tokenizertest.pickle', 'rb') as handle:
            Tokenizer = pickle.load(handle)

        # Load model

        print("[INFO]: Loading Model")
        model = tf.keras.models.load_model("./model_final.model")

        print("[INFO]: Preprocessing")

        # Preprocess Text

        def preprocess_text(sentence):
            import string
            arabic_punctuations = '''`÷×#@؛<>_()*&^%][ـ،/:"؟.,'{}~¦+|!”…“–ـ'''
            english_punctuations = string.punctuation
            punctuations_list = arabic_punctuations + english_punctuations

            def lower(text):
                res = ''
                for ligne in text:
                    res = res + ligne.lower()
                return res

            def remove_punctuations(text):
                translator = str.maketrans('', '', punctuations_list)
                return text.translate(translator)

            def remove_repeating_char(text):
                return re.sub(r'(.)\1+', r'\1\1', text)

            def remove_extra_whitespace(string):
                red = re.sub(' +', ' ', string)
                return red

            def remove_urls(string):
                string = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', string, flags=re.MULTILINE)
                return string

            def remove_dates(string):
                OutputList = filter(
                    lambda ThisWord: not re.match('^(?:(?:[0-9]{2}[:\/,]){2}[0-9]{2,4}|am|pm)$', ThisWord),
                    string)
                for ThisValue in OutputList:
                    print(ThisValue)

            def remove_non_arabic_symbols(string):  # marche seulement pour les mots arabes
                return re.sub(r'[^\u0600-\u06FF]', ' ', string)

            def remove_numbers(string):  # supprime meme les nombre dans une mot comme m9ate3 devient m ate
                regex = re.compile(r"(\d|[\u0660\u0661\u0662\u0663\u0664\u0665\u0666\u0667\u0668\u0669])+")
                return re.sub(regex, ' ', string)

            def removeonecarc(string):
                for ligne1 in string:
                    resul = re.sub(r"\s+[a-zA-Z]\s+", " ", ligne1)  # Removing a Single Character
                return resul

            def remove_numbre_not_att(string):  # marche seulement pour les mots arabes
                return re.sub(r'\b[0-9]+\b\s*', '', string)

            text2 = remove_urls(sentence)
            text3 = remove_numbre_not_att(text2)
            text4 = remove_punctuations(text3)
            text6 = lower(text4)
            text7 = remove_extra_whitespace(text6)
            for ligne1 in [text7]:
                resul = re.sub(r"\s+[a-zA-Z]\s+", " ", ligne1)  # Removing a Single Character 11

            for ligne in [resul]:
                resull = re.sub(r"\s+[a-zA-Z]\s+", " ", ligne)  # Removing a Single Character 22

            sentence = resull

            return sentence

        # if request.post == "POST":
        # message = request['message']
        MAX_LENGTH = 100
        sentence_processed = Tokenizer.texts_to_sequences([sentence])
        sentence_processed = np.array(sentence_processed)
        sentence_padded = tf.keras.preprocessing.sequence.pad_sequences(sentence_processed, padding='post',
                                                                        maxlen=MAX_LENGTH)

        print("""[INFO]: Prediction\n\t{}""".format(sentence[0]))
        # Get prediction for sentence
        # import pdb;pdb.set_trace()
        result = model.predict(sentence_padded)
        result_list = result.tolist()
        print(result_list)
        print("-" * 20)
        # Show prediction
        print("[INFO]: class for given text is: {}".format(classNames[np.argmax(result)]))
        pred = classNames[np.argmax(result)]
        return render_template("sentiment.html", prediction=result.all(), result_list=result_list, pred=pred)


if __name__ == "__main__":
    app.run(port=5001, debug=True)
