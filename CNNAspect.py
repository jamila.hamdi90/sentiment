import pandas as pd  
import numpy as np
from keras.preprocessing.sequence import pad_sequences
import keras.layers
from keras.layers import Embedding, Reshape
from keras.layers import Input
import matplotlib.pyplot as plt
#dataset = pd.read_csv('testArabe.csv', sep=';', names=['sentiment', 'aspect_category', 'review','aspect_terms', 'sentiment_terms'], encoding="Windows-1256")
dataset = pd.read_csv('corpusArFi.csv', sep=';', encoding="Windows-1256", header= None)
dataset = dataset.loc[:, [0, 1, 2, 3, 4]]
dataset = dataset.rename(index=str, columns={0:'sentiment', 1:'aspect_category', 2:'review',3:'aspect_terms', 4:'sentiment_terms'})
dataset.head(100)
# reviews_train.columns
print(dataset.groupby('aspect_category').size().sort_values(ascending=False))
#how many categories
print("number of categories",dataset.aspect_category.nunique())

label = dataset['sentiment'].values
print(label)
print(dataset[dataset['sentiment'] == 'positif'].size)
print(dataset[dataset['sentiment'] == 'négatif'].size)
print(dataset[dataset['sentiment'] == 'très positif'].size)
print(dataset[dataset['sentiment'] == 'très négatif'].size)
print(dataset[dataset['sentiment'] == 'neutre'].size)

review = dataset['review']
aspect_terms= dataset['aspect_terms']
aspect_category= dataset['aspect_category']
from sklearn.model_selection import train_test_split
# encode the aspect_category :
import keras
from sklearn import preprocessing
# encode class values as integers
encoder = preprocessing.LabelEncoder()
encoder.fit(aspect_category.astype(str))
encoded_Y = encoder.transform(aspect_category.astype(str))
# convert integers to dummy variables (i.e. one hot encoded)
dummy_category = keras.utils.to_categorical(encoded_Y)
print(dummy_category.shape)
aspect_train, aspect_test, y_train, y_test = train_test_split(aspect_terms, dummy_category, test_size=0.25, random_state=1000)

#Tokeniser
from keras.preprocessing.text import Tokenizer
vocab_size = 40000 # We set a maximum size for the vocabulary
tokenizer = Tokenizer(num_words=vocab_size, lower=False)

tokenizer.fit_on_texts(review.astype(str))
aspect_train = tokenizer.texts_to_sequences(aspect_terms.astype(str))
aspect_test = tokenizer.texts_to_sequences(aspect_terms.astype(str))
#aspect_tokenized = pd.DataFrame(tokenizer.texts_to_matrix(aspect_terms.astype(str)))
word_index = tokenizer.word_index
vocab_size = len(word_index) + 1  # Adding 1 because of reserved 0 index
print('Found %s unique tokens.' % len(word_index))
#pad_sequences
maxlen = 1000
aspect_train = pad_sequences(aspect_train, padding='post', maxlen=maxlen)
aspect_test = pad_sequences(aspect_test, padding='post', maxlen=maxlen)
print('Shape of data tensor:', aspect_train.shape)

import nltk
nltk.download('punkt')
wtxt1 = open('arabeF.txt', encoding="Windows-1256").read()
wtokens = nltk.word_tokenize(wtxt1)
import logging 
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logging.info ("Done reading data file")
from gensim.models import Word2Vec
model = Word2Vec ([wtokens], size=300, window=10,  iter=10, min_count=1, workers=10) #min_count means ignore all words with total frequency lower than this. 
model.save("word2vec.model")
print("Finished!")
word_vectors = Word2Vec.load("word2vec.model")
len(model.wv.vocab)
num_words=40000
EMBEDDING_DIM=300
vocab_size=40000
vocabulary_size=min(vocab_size,num_words)
embedding_matrix = np.zeros((vocabulary_size, EMBEDDING_DIM))
print('Shape of embedding_matrix:', embedding_matrix.shape)

for word, i in word_index.items():
    if i>=num_words:
        continue
    try:
        embedding_vector = word_vectors[word]
        embedding_matrix[i] = embedding_vector
    except KeyError:
        embedding_matrix[i]=np.random.normal(0,np.sqrt(0.25),EMBEDDING_DIM)
del(word_vectors)

#Embedding
embedding_layer = Embedding(vocabulary_size,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            trainable=True)

# Convolutional Neural Networks (CNN)
from keras.models import Sequential
from keras import layers

embedding_dim = 300
#je dois essayer de changer embedding zite to 300
aspect_categories_model = Sequential()
aspect_categories_model.add(embedding_layer)
#model.add(layers.Conv1D(23, 5, activation='relu'))
#model.add(layers.Conv1D(64, 5, activation='relu'))
aspect_categories_model.add(layers.Conv1D(128, 5, activation='relu'))
aspect_categories_model.add(layers.GlobalMaxPooling1D())
#model.add(layers.Conv1D(128, 5, activation='relu'))
#model.add(layers.GlobalMaxPooling1D())
aspect_categories_model.add(layers.Dense(10, activation='relu'))
#model.add(Dropout(0.5))
aspect_categories_model.add(layers.Dense(29, activation='softmax'))
aspect_categories_model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['categorical_accuracy'])
aspect_categories_model.summary()

es_callback = keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
#aspect_train = pd.DataFrame(aspect_train) ////////////////////
aspect_tokenized = pd.DataFrame(tokenizer.texts_to_matrix(aspect_terms.astype(str)))
history = aspect_categories_model.fit(aspect_train, dummy_category, epochs=100,validation_split=0.2, batch_size=64, callbacks=[es_callback])
aspect_categories_model.predict(aspect_test) #y_test n'entre pas dans la prédiction

#Test of aspect_categories_model 
new_review = "ابربي وين عنوانها في سوسة"
new_review_aspect_terms = 'سوسة'
new_review_aspect_tokenized = tokenizer.texts_to_matrix([new_review_aspect_terms])
new_review_category = encoder.inverse_transform(aspect_categories_model.predict_classes(new_review_aspect_tokenized))
print(new_review_category)

#step4: build the sentiment model
sentiment_terms= dataset['sentiment_terms']
sentiment= dataset['sentiment']

from sklearn.model_selection import train_test_split
# encode the aspect_category :
import keras
from sklearn import preprocessing
# encode class values as integers
label_encoder_2 = preprocessing.LabelEncoder()
label_encoder_2.fit(sentiment.astype(str))
encoded_Y = encoder.transform(sentiment.astype(str))
# convert integers to dummy variables (i.e. one hot encoded)
dummy_sentiment = keras.utils.to_categorical(encoded_Y)
print(dummy_sentiment.shape)
aspect_train, aspect_test, y_train, y_test = train_test_split(sentiment_terms, dummy_sentiment, test_size=0.25, random_state=1000)

#Tokeniser
from keras.preprocessing.text import Tokenizer
vocab_size = 40000 # We set a maximum size for the vocabulary
tokenizer = Tokenizer(num_words=vocab_size, lower=False)

tokenizer.fit_on_texts(review.astype(str))
sentiment_train = tokenizer.texts_to_sequences(sentiment_terms.astype(str))
sentiment_test = tokenizer.texts_to_sequences(sentiment_terms.astype(str))
#aspect_tokenized = pd.DataFrame(tokenizer.texts_to_matrix(aspect_terms.astype(str)))
word_index = tokenizer.word_index
vocab_size = len(word_index) + 1  # Adding 1 because of reserved 0 index

#pad_sequences
maxlen = 1000
sentiment_train = pad_sequences(sentiment_terms, padding='post', maxlen=maxlen)
sentiment_test = pad_sequences(sentiment_terms, padding='post', maxlen=maxlen)
print('Shape of data tensor:', sentiment_train.shape)

#je dois essayer de changer embedding size to 300
sentiment_model = Sequential()
sentiment_model.add(embedding_layer)
#model.add(layers.Conv1D(23, 5, activation='relu'))
#model.add(layers.Conv1D(64, 5, activation='relu'))
sentiment_model.add(layers.Conv1D(128, 5, activation='relu'))
sentiment_model.add(layers.GlobalMaxPooling1D())
#model.add(layers.Conv1D(128, 5, activation='relu'))
#model.add(layers.GlobalMaxPooling1D())
sentiment_model.add(layers.Dense(10, activation='relu'))
#model.add(Dropout(0.5))
sentiment_model.add(layers.Dense(5, activation='softmax'))
sentiment_model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['categorical_accuracy'])
sentiment_model.summary()

es_callback = keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
history = model.fit(sentiment_train, dummy_category, epochs=100,validation_split=0.2, batch_size=64, callbacks=[es_callback])
model.predict(sentiment_test)

new_review = "المغازة العامة ادلالها ما يوفاش"
new_review_aspect_terms = 'المغازة العامة'
new_review_aspect_tokenized = tokenizer.texts_to_matrix([new_review_aspect_terms])

new_review_category = label_encoder_2.inverse_transform(sentiment_model.predict_classes(new_review_aspect_tokenized))
print(new_review_category)
import pandas as pd  
import numpy as np
from keras.preprocessing.sequence import pad_sequences
import keras.layers
from keras.layers import Embedding, Reshape
from keras.layers import Input
import matplotlib.pyplot as plt
#dataset = pd.read_csv('testArabe.csv', sep=';', names=['sentiment', 'aspect_category', 'review','aspect_terms', 'sentiment_terms'], encoding="Windows-1256")
dataset = pd.read_csv('corpusArFi.csv', sep=';', encoding="Windows-1256", header= None)
dataset = dataset.loc[:, [0, 1, 2, 3, 4]]
dataset = dataset.rename(index=str, columns={0:'sentiment', 1:'aspect_category', 2:'review',3:'aspect_terms', 4:'sentiment_terms'})
dataset.head(100)
# reviews_train.columns
print(dataset.groupby('aspect_category').size().sort_values(ascending=False))
#how many categories
print("number of categories",dataset.aspect_category.nunique())

label = dataset['sentiment'].values
print(label)
print(dataset[dataset['sentiment'] == 'positif'].size)
print(dataset[dataset['sentiment'] == 'négatif'].size)
print(dataset[dataset['sentiment'] == 'très positif'].size)
print(dataset[dataset['sentiment'] == 'très négatif'].size)
print(dataset[dataset['sentiment'] == 'neutre'].size)

review = dataset['review']
aspect_terms= dataset['aspect_terms']
aspect_category= dataset['aspect_category']
from sklearn.model_selection import train_test_split
# encode the aspect_category :
import keras
from sklearn import preprocessing
# encode class values as integers
encoder = preprocessing.LabelEncoder()
encoder.fit(aspect_category.astype(str))
encoded_Y = encoder.transform(aspect_category.astype(str))
# convert integers to dummy variables (i.e. one hot encoded)
dummy_category = keras.utils.to_categorical(encoded_Y)
print(dummy_category.shape)
aspect_train, aspect_test, y_train, y_test = train_test_split(aspect_terms, dummy_category, test_size=0.25, random_state=1000)

#Tokeniser
from keras.preprocessing.text import Tokenizer
vocab_size = 40000 # We set a maximum size for the vocabulary
tokenizer = Tokenizer(num_words=vocab_size, lower=False)

tokenizer.fit_on_texts(review.astype(str))
aspect_train = tokenizer.texts_to_sequences(aspect_terms.astype(str))
aspect_test = tokenizer.texts_to_sequences(aspect_terms.astype(str))
#aspect_tokenized = pd.DataFrame(tokenizer.texts_to_matrix(aspect_terms.astype(str)))
word_index = tokenizer.word_index
vocab_size = len(word_index) + 1  # Adding 1 because of reserved 0 index
print('Found %s unique tokens.' % len(word_index))
#pad_sequences
maxlen = 1000
aspect_train = pad_sequences(aspect_train, padding='post', maxlen=maxlen)
aspect_test = pad_sequences(aspect_test, padding='post', maxlen=maxlen)
print('Shape of data tensor:', aspect_train.shape)

import nltk
nltk.download('punkt')
wtxt1 = open('arabeF.txt', encoding="Windows-1256").read()
wtokens = nltk.word_tokenize(wtxt1)
import logging 
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logging.info ("Done reading data file")
from gensim.models import Word2Vec
model = Word2Vec ([wtokens], size=300, window=10,  iter=10, min_count=1, workers=10) #min_count means ignore all words with total frequency lower than this. 
model.save("word2vec.model")
print("Finished!")
word_vectors = Word2Vec.load("word2vec.model")
len(model.wv.vocab)
num_words=40000
EMBEDDING_DIM=300
vocab_size=40000
vocabulary_size=min(vocab_size,num_words)
embedding_matrix = np.zeros((vocabulary_size, EMBEDDING_DIM))
print('Shape of embedding_matrix:', embedding_matrix.shape)

for word, i in word_index.items():
    if i>=num_words:
        continue
    try:
        embedding_vector = word_vectors[word]
        embedding_matrix[i] = embedding_vector
    except KeyError:
        embedding_matrix[i]=np.random.normal(0,np.sqrt(0.25),EMBEDDING_DIM)
del(word_vectors)

#Embedding
embedding_layer = Embedding(vocabulary_size,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            trainable=True)

# Convolutional Neural Networks (CNN)
from keras.models import Sequential
from keras import layers

embedding_dim = 300
#je dois essayer de changer embedding zite to 300
aspect_categories_model = Sequential()
aspect_categories_model.add(embedding_layer)
#model.add(layers.Conv1D(23, 5, activation='relu'))
#model.add(layers.Conv1D(64, 5, activation='relu'))
aspect_categories_model.add(layers.Conv1D(128, 5, activation='relu'))
aspect_categories_model.add(layers.GlobalMaxPooling1D())
#model.add(layers.Conv1D(128, 5, activation='relu'))
#model.add(layers.GlobalMaxPooling1D())
aspect_categories_model.add(layers.Dense(10, activation='relu'))
#model.add(Dropout(0.5))
aspect_categories_model.add(layers.Dense(29, activation='softmax'))
aspect_categories_model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['categorical_accuracy'])
aspect_categories_model.summary()

es_callback = keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
#aspect_train = pd.DataFrame(aspect_train) ////////////////////
aspect_tokenized = pd.DataFrame(tokenizer.texts_to_matrix(aspect_terms.astype(str)))
history = aspect_categories_model.fit(aspect_train, dummy_category, epochs=100,validation_split=0.2, batch_size=64, callbacks=[es_callback])
aspect_categories_model.predict(aspect_test) #y_test n'entre pas dans la prédiction

#Test of aspect_categories_model 
new_review = "ابربي وين عنوانها في سوسة"
new_review_aspect_terms = 'سوسة'
new_review_aspect_tokenized = tokenizer.texts_to_matrix([new_review_aspect_terms])
new_review_category = label_encoder.inverse_transform(aspect_categories_model.predict_classes(new_review_aspect_tokenized))
print(new_review_category)

#step4: build the sentiment model
sentiment_terms= dataset['sentiment_terms']
sentiment= dataset['sentiment']

from sklearn.model_selection import train_test_split
# encode the aspect_category :
import keras
from sklearn import preprocessing
# encode class values as integers
label_encoder_2 = preprocessing.LabelEncoder()
label_encoder_2.fit(sentiment.astype(str))
encoded_Y = encoder.transform(sentiment.astype(str))
# convert integers to dummy variables (i.e. one hot encoded)
dummy_sentiment = keras.utils.to_categorical(encoded_Y)
print(dummy_sentiment.shape)
aspect_train, aspect_test, y_train, y_test = train_test_split(sentiment_terms, dummy_sentiment, test_size=0.25, random_state=1000)

#Tokeniser
from keras.preprocessing.text import Tokenizer
vocab_size = 40000 # We set a maximum size for the vocabulary
tokenizer = Tokenizer(num_words=vocab_size, lower=False)

tokenizer.fit_on_texts(review.astype(str))
sentiment_train = tokenizer.texts_to_sequences(sentiment_terms.astype(str))
sentiment_test = tokenizer.texts_to_sequences(sentiment_terms.astype(str))
#aspect_tokenized = pd.DataFrame(tokenizer.texts_to_matrix(aspect_terms.astype(str)))
word_index = tokenizer.word_index
vocab_size = len(word_index) + 1  # Adding 1 because of reserved 0 index

#pad_sequences
maxlen = 1000
sentiment_train = pad_sequences(sentiment_terms, padding='post', maxlen=maxlen)
sentiment_test = pad_sequences(sentiment_terms, padding='post', maxlen=maxlen)
print('Shape of data tensor:', sentiment_train.shape)

#je dois essayer de changer embedding zite to 300
sentiment_model = Sequential()
sentiment_model.add(embedding_layer)
#model.add(layers.Conv1D(23, 5, activation='relu'))
#model.add(layers.Conv1D(64, 5, activation='relu'))
sentiment_model.add(layers.Conv1D(128, 5, activation='relu'))
sentiment_model.add(layers.GlobalMaxPooling1D())
#model.add(layers.Conv1D(128, 5, activation='relu'))
#model.add(layers.GlobalMaxPooling1D())
sentiment_model.add(layers.Dense(10, activation='relu'))
#model.add(Dropout(0.5))
sentiment_model.add(layers.Dense(5, activation='softmax'))
sentiment_model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['categorical_accuracy'])
sentiment_model.summary()

es_callback = keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
history = model.fit(sentiment_train, dummy_category, epochs=100,validation_split=0.2, batch_size=64, callbacks=[es_callback])
model.predict(sentiment_test)

new_review = "المغازة العامة ادلالها ما يوفاش"
new_review_aspect_terms = 'المغازة العامة'
new_review_aspect_tokenized = tokenizer.texts_to_matrix([new_review_aspect_terms])

new_review_category = label_encoder_2.inverse_transform(sentiment_model.predict_classes(new_review_aspect_tokenized))
print(new_review_category)
plot_history(history)