import pandas as pd
import numpy as np
import re
import pickle
import nltk
# from nltk.corpus import stopwords
import keras
from keras.preprocessing.text import one_hot
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers.core import Activation, Dropout, Dense
from keras.layers import Flatten, LSTM, Bidirectional, Conv1D, GRU
from keras.layers import GlobalMaxPooling1D, GlobalAveragePooling1D
from keras.layers.embeddings import Embedding
from sklearn.model_selection import train_test_split
from keras.preprocessing.text import Tokenizer
from sklearn.preprocessing import LabelBinarizer

print("INFO: Loading Dataset")
# Load dataset
#dataset = pd.read_csv("dataset/iseardataset.csv")
filepath_dict = {'ArabComments':   'latinArabe1.csv' }

df_list = []
for source, filepath in filepath_dict.items():
    df = pd.read_csv(filepath, names=['sentence', 'label'], sep=';',  encoding="latin-1")
    df['source'] = source  # Add another column filled with the source name
    df_list.append(df)

df = pd.concat(df_list)
#print(df.iloc[0])


def preprocess_text(sen):
   
    import string
    arabic_punctuations = '''`÷×#@؛<>_()*&^%][ـ،/:"؟.,'{}~¦+|!”…“–ـ'''
    english_punctuations = string.punctuation
    punctuations_list = arabic_punctuations + english_punctuations

    def lower(text):
        res=''
        for ligne in text:
            res=res+ligne.lower()
        return res

    def remove_punctuations(text):
        translator = str.maketrans('', '', punctuations_list)
        return text.translate(translator)
    
    def remove_repeating_char(text):
        return re.sub(r'(.)\1+', r'\1\1', text)


    def remove_extra_whitespace(string):
        red = re.sub(' +',' ',string)
        return red
    
    def remove_urls(string):
        string = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', string, flags=re.MULTILINE)
        return string

    def remove_dates(string):
        OutputList = filter(lambda ThisWord: not re.match('^(?:(?:[0-9]{2}[:\/,]){2}[0-9]{2,4}|am|pm)$', ThisWord),
        string)
        for ThisValue in OutputList:
            print (ThisValue)

    def remove_non_arabic_symbols(string): #marche seulement pour les mots arabes
        return re.sub(r'[^\u0600-\u06FF]', ' ', string)    
    
    def remove_numbers(string):   #supprime meme les nombre dans une mot comme m9ate3 devient m ate
        regex = re.compile(r"(\d|[\u0660\u0661\u0662\u0663\u0664\u0665\u0666\u0667\u0668\u0669])+")
        return re.sub(regex, ' ', string)
    
    def removeonecarc(string): 
        for ligne1 in string:
            resul = re.sub(r"\s+[a-zA-Z]\s+", " ", ligne1)#Removing a Single Character
        return resul
    
    def remove_numbre_not_att(string): #marche seulement pour les mots arabes
        return re.sub(r'\b[0-9]+\b\s*', '', string)   
             
    text2 = remove_urls(sen) 
    text3 = remove_numbre_not_att(text2)
    text4 = remove_punctuations(text3)
    text6 = lower(text4)
    text7 = remove_extra_whitespace(text6)
    for ligne1 in [text7]:
        resul = re.sub(r"\s+[a-zA-Z]\s+", " ", ligne1)#Removing a Single Character 11  
       
    for ligne in [resul]:
        resull = re.sub(r"\s+[a-zA-Z]\s+", " ", ligne)#Removing a Single Character 22
    
    sentence = resull 

    return sentence


print("INFO: Preprocessing Dataset")
# Preprocess sentences
#from sklearn.model_selection import train_test_split
#df_yelp = df[df['source'] == 'ArabComments']
#sentences = df_yelp['sentence'].values
#y = df_yelp['label'].values

X = []
sentences = list(df['sentence'])

for sen in sentences:
    X.append(preprocess_text(sen))

y = df['label']
from sklearn import preprocessing
# encode class values as integers
encoder = preprocessing.LabelEncoder()
encoder.fit(y)
encoded_Y = encoder.transform(y)
# convert integers to dummy variables (i.e. one hot encoded)
y = keras.utils.to_categorical(encoded_Y)
# Binarize labels with SKLearn label binarizer
#encoder = LabelBinarizer()
#y = encoder.fit_transform(y)

# Split train and test data
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=1000)

# Tokenize sentencs to numbers with max number 10000
num_words=200000 #45000
tokenizer = Tokenizer(num_words)
tokenizer.fit_on_texts(X_train)

X_train = tokenizer.texts_to_sequences(X_train)
X_test = tokenizer.texts_to_sequences(X_test)
word_index = tokenizer.word_index
vocab_size = len(word_index) + 1
# Adding 1 because of reserved 0 index


maxlen = 100

# Pad sequences to max length with post padding.
X_train = pad_sequences(X_train, padding='post', maxlen=maxlen)
X_test = pad_sequences(X_test, padding='post', maxlen=maxlen)


nltk.download('punkt')
wtxt1 = open('latinarabe.txt', encoding='latin1').read()
wtokens = nltk.word_tokenize(wtxt1)

import logging 
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logging.info ("Done reading data file")
from gensim.models import Word2Vec
model = Word2Vec ([wtokens], size=100, window=10,  iter=10, min_count=1, workers=10,sg=1) #min_count means ignore all words with total frequency lower than this.
#for chh in wtokens:
 #   vectoR = model.wv[chh] 
  #  print(vectoR)   # numpy vector of a word    
model.save("word2vec.model")
#print("Finished!")
word_vectors = Word2Vec.load("word2vec.model")
#len(model.wv.vocab)

EMBEDDING_DIM=100
vocab_size=200000
vocabulary_size=min(vocab_size,num_words)
embedding_matrix = np.zeros((vocabulary_size, EMBEDDING_DIM))
#print('Shape of embedding_matrix:', embedding_matrix.shape)

for word, i in word_index.items():
    if i>=num_words:
        continue
    try:
        embedding_vector = word_vectors[word]
        embedding_matrix[i] = embedding_vector
    except KeyError:
        embedding_matrix[i]=np.random.normal(0,np.sqrt(0.25),EMBEDDING_DIM)
del(word_vectors)

#Embedding
embedding_layer = Embedding(vocabulary_size,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            trainable=True)
#####
# Load embedding file

print("INFO: Creating model")
# Create a Keras CNN model 
from keras import layers

embedding_dim = 100
#je dois essayer de changer embedding zite to 300

model = Sequential()
model.add(embedding_layer)
#model.add(layers.Conv1D(23, 5, activation='relu'))
#model.add(layers.Conv1D(64, 5, activation='relu'))
model.add(layers.Conv1D(128, 5, activation='relu'))
model.add(layers.GlobalMaxPooling1D())
#model.add(layers.Conv1D(128, 5, activation='relu'))
#model.add(layers.GlobalMaxPooling1D())
model.add(layers.Dense(128, activation='relu'))
#model.add(Dropout(0.5))
model.add(layers.Dense(5, activation='softmax'))

# Compile model
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['categorical_accuracy'])
print("INFO: Starting training")
# Train/fit model on dataset
import keras
es_callback = keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)
history = model.fit(X_train, y_train, epochs=5, validation_split=0.2,validation_data=[X_test, y_test], batch_size=84, callbacks=[es_callback])
model.score(X_test, y_test)
# Save model, classes_names and tokenizer file
model.save("model_final.model")
np.save("class_namestest.npy", encoder.classes_)

with open('tokenizertest.pickle', 'wb') as handle:
    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
print("INFO: Saving models done")
